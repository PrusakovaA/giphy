package com.bsa.giphy.service;

import com.bsa.giphy.exception.InvalidException;
import com.bsa.giphy.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public String generateGif(Optional<Boolean> force, String query, String userId) {
        if (query.trim().length() != 0) {
            return force.isPresent() ? userRepository.getGifFromUrl(query, userId)
                    : userRepository.getGifFromCache(query, userId);
        } else {
            throw new InvalidException("Invalid request: " + query);
        }
    }

    public ResponseEntity<List<Map<String, Object>>> getAllFiles(String userId) {
        return userRepository.getAllFiles(userId);
    }

    public File getUserHistory(String userId) {
        return userRepository.getUserHistory(userId);
    }

    public void cleanUserHistory(String userId) {
        userRepository.cleanUserHistory(userId);
    }


    public void deleteUser(String userId) {
        userRepository.deleteUser(userId);
    }


}

