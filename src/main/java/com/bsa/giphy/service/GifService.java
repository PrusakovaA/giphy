package com.bsa.giphy.service;

import com.bsa.giphy.repository.GifRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GifService {

    @Autowired
    private GifRepository gifRepository;

    public List<String> getAllGifs() {
        return gifRepository.getAllGifs();
    }
}
