package com.bsa.giphy.repository;

import com.bsa.giphy.exception.NotFoundException;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Repository
public class GifRepository {

    private final String STORAGE_PATH = "..\\cache";

    public List<String> getAllGifs() {
        try (Stream<Path> paths = Files.walk(Paths.get(STORAGE_PATH))) {
            List<String> output = paths
                    .filter(Files::isRegularFile)
                    .map(Path::toString)
                    .collect(Collectors.toList());
            return output;
        } catch (IOException e) {
            throw new NotFoundException("No files in the cache directory");
        }
    }

}
