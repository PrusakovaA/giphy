package com.bsa.giphy.repository;

import com.bsa.giphy.exception.NotFoundException;
import com.bsa.giphy.util.GetGif;
import com.bsa.giphy.util.GifMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Repository
public class CacheRepository {

    private final String STORAGE_PATH = "..\\cache";

    @Autowired
    private GetGif getGif;

    @Autowired
    private GifMapper gifMapper;

    public void clearCache() {
        try {
            File cachePath = new File(STORAGE_PATH);
            cachePath.delete();
        } catch (Exception e) {
            throw new NotFoundException("The directory doesn't exist");
        }
    }


    public String generateCache(String query, int counter) {
        if (counter <= 10) {
            return getGif.getGifReceiveDto(query)
                    .map(imageReceiveDto -> {
                        try {
                            return gifMapper
                                    .gifFromDto(imageReceiveDto, query,  Optional.empty());
                        } catch (IOException e) {
                            return null;
                        }
                    })
                    .map(gif -> gif.getGif()
                            .map(File::getPath)
                            .orElseThrow())
                    .orElseGet(() -> generateCache(query, counter + 1 ));
        } else {
            throw new RuntimeException("Can't  find " + query );
        }

    }

    public ResponseEntity<List<Map<String, Object>>> getAllCache(String query) {
            try {

            List<Map<String, Object>> list = new ArrayList<>();
            list.add(new HashMap<>(Map.of("query", query)));
            list.get(0).put("gifs", Files.walk(Paths.get(STORAGE_PATH + query))
                    .filter(Files::isRegularFile).map(Path::toString).collect(Collectors.toList()));

            return ResponseEntity.status(HttpStatus.OK).body(list);
        } catch (IOException e) {
            throw new NotFoundException("Can`t find the folder");
        }
    }

    public ResponseEntity<List<Map<String, Object>>> getAllCache() {
        try {
            Stream<String> paths = Optional.of(new File(STORAGE_PATH))
                    .map(file -> file.listFiles(File::isDirectory)).map(Arrays::asList)
                    .orElseThrow(() -> new NotFoundException("Cache folder is clear")).stream().map(File::getName);
            List<Map<String, Object>> list = new ArrayList<>();
            paths.forEach(path -> list.add(new HashMap<>(Map.of("query", path))));
            for (Map<String, Object> map : list) {
                map.put("gifs", Files
                        .walk(Paths.get(STORAGE_PATH + map.get("query")))
                        .filter(Files::isRegularFile)
                        .map(Path::normalize)
                        .map(Path::toString)
                        .collect(Collectors.toList()));
            }
            return ResponseEntity.status(HttpStatus.OK).body(list);

        } catch (IOException e) {
            throw new NotFoundException("Can`t find the folder");
        }
    }


}
