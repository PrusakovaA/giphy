package com.bsa.giphy.repository;

import com.bsa.giphy.exception.InvalidException;
import com.bsa.giphy.util.GetGif;
import com.bsa.giphy.util.GifMapper;
import com.bsa.giphy.util.UploadGif;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Repository
public class UserRepository {

    private final String STORAGE_PATH = "..\\cache\\";

    @Autowired
    private GifMapper imageMapper;

    @Autowired
    private UploadGif uploadGif;

    @Autowired
    private GetGif getGif;


    public String getGifFromCache(String query, String userId) {
        try (Stream<Path> paths = Files.walk(Paths.get(STORAGE_PATH + query))) {
            Optional<Path> file = paths.filter(Files::isRegularFile).findAny();
            if (file.isPresent()) {
                uploadGif.putImageToUserFolder(file.get(), userId);
            }
            return file.map(path -> path.normalize().toString())
                    .orElseGet(() -> getGifFromUrl(query, userId));
        } catch (IOException e) {
            return getGifFromUrl(query, userId);
        }

    }

    public String getGifFromUrl(String query, String userId) {
        return getGif.getGifReceiveDto(query)
                .map(imageReceiveDto -> {
                    try {
                        return imageMapper
                                .gifFromDto(imageReceiveDto, query, Optional.of(userId));
                    } catch (IOException e) {
                        return null;
                    }
                })
                .map(gif -> gif.getGif()
                        .map(File::getPath)
                        .orElseThrow())
                .orElseGet(() -> getGifFromUrl(query, userId));

    }

    public File getUserHistory(String userId) {
        return new File(STORAGE_PATH + "users\\" + userId + "\\history.csv");
    }

    public void cleanUserHistory(String userId) {
        try {
            Files.deleteIfExists(Paths.get(STORAGE_PATH + "users\\" + userId + "\\history.csv"));
        } catch (IOException e) {

        }
    }

    public void deleteUser(String user_id) {
        File userCache = new File(STORAGE_PATH + "users\\" + user_id);
        try {
            FileUtils.forceDelete(userCache);
        } catch (IOException ex) {
            System.out.println(ex);
        }
    }

    public ResponseEntity<List<Map<String, Object>>> getAllFiles(String userId) {
        try (Stream<String> paths = Optional
                .of(new File(STORAGE_PATH + "users\\" + userId))
                .map(file -> file.listFiles(File::isDirectory)).map(Arrays::asList)
                .orElseThrow(() -> new InvalidException("Not found: " + userId)).stream().map(File::getName)) {

            List<Map<String, Object>> list = new ArrayList<>();
            paths.forEach(path -> list.add(new HashMap<>(Map.of("query", path))));

            for (Map<String, Object> map : list) {
                map.put("gifs", Files
                        .walk(Paths.get(STORAGE_PATH + "users\\" + userId + "/" + map.get("query")))
                        .filter(Files::isRegularFile).map(Path::normalize).map(Path::toString)
                        .collect(Collectors.toList()));
            }
            return ResponseEntity.status(HttpStatus.OK).body(list);
        } catch (IOException e) {
            throw new InvalidException("Not found: " + userId);
        }
    }

}
