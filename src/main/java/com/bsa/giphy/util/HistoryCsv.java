package com.bsa.giphy.util;

import com.bsa.giphy.exception.InvalidException;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;
import org.springframework.stereotype.Component;

import java.io.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class HistoryCsv {

    private final String STORAGE_PATH = "..\\cache\\users";

    public void writeHistory(LocalDate date, String userId, String query, String filePath) {
        try {
            BufferedWriter writer = new BufferedWriter(
                    new FileWriter(STORAGE_PATH + "\\" + userId + "\\history.csv", true));
            CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT);

            csvPrinter.printRecord(date, query, filePath);
            csvPrinter.flush();

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public List<Map<String, String>> readHistory(String userId) {
        try (
                Reader reader = new BufferedReader(
                        new FileReader(STORAGE_PATH + "\\" + userId + "\\history.csv"));
                CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT);
        ) {
            List<Map<String, String>> history = new ArrayList<>();
            for (CSVRecord record : csvParser) {
                history.add(Map.of("date", record.get(0), "query", record.get(1), "gif", record.get(2)));
            }
            return history;
        } catch (IOException e) {
            throw new InvalidException("Can't find such user: " + userId);
        }
    }
}
