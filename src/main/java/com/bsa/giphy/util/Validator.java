package com.bsa.giphy.util;

import com.bsa.giphy.exception.InvalidException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

@Component
public class Validator {
    public void validate(String variable, String... variables) {
        List<String> list = new ArrayList<>(Arrays.asList(variables));
        list.add(variable);
        Pattern pattern = Pattern.compile("[\\|.|\\s|\b|\n]");
        for (String v : list) {
            if (pattern.matcher(v).find()) {
                throw new InvalidException("Invalid : " + v);
            }
        }
    }
 }
