package com.bsa.giphy.util;

import com.bsa.giphy.dto.GetGifDto;
import com.bsa.giphy.exception.NotFoundException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.net.URL;
import java.util.Optional;

public class GetGif {

    private final String url;
    private final String apiKey;

    public GetGif(String url, String apiKey) {
        this.url = url;
        this.apiKey = apiKey;
    }


    @Autowired
    private ObjectMapper mapper;

    public Optional<GetGifDto> getGifReceiveDto(final String query) {
        try {
            JsonNode root = mapper
                    .readTree(new URL(url + "?api_key=" + apiKey + "&tag=" + query))
                    .path("data");
            GetGifDto getGifDto = new GetGifDto();
            getGifDto.setId(root.path("id").asText());
            getGifDto.setUrl(new URL(root.path("images").path("downsized_large").path("url").asText()));
            return Optional.of(getGifDto);
        } catch (IOException e) {
            throw new NotFoundException("Can't find " + query);
        }
    }
}
