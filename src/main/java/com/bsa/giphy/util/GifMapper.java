package com.bsa.giphy.util;

import com.bsa.giphy.dto.GetGifDto;
import com.bsa.giphy.entity.Gif;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Optional;

@Component
public class GifMapper {
    @Autowired
    UploadGif uploadGif;

    public Gif gifFromDto(GetGifDto getGifDto, String userId, Optional<String> query) throws IOException {
        return Gif.builder().userId(userId).query(query)
                .gif(uploadGif.uploadGif(getGifDto, userId, query)).name(getGifDto.getId()).build();
    }
}
