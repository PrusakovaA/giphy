package com.bsa.giphy.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
@Slf4j
public class HeaderFilter implements Filter {

    @Override
    public void doFilter(
            ServletRequest request,
            ServletResponse response,
            FilterChain chain) throws IOException, ServletException {

        HttpServletResponse res = (HttpServletResponse) response;
        if (!"X-BSA-GIPHY".equals(((HttpServletRequest) request).getHeader("X-BSA-GIPHY"))) {
            res.sendError(HttpStatus.FORBIDDEN.value(), "Invalid or missing header");
            log.error(request.getRemoteAddr() + " incorrect header!");
        }
    }

}
