package com.bsa.giphy.util;

import com.bsa.giphy.dto.GetGifDto;
import com.bsa.giphy.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDate;
import java.util.Optional;

@Component
public class UploadGif {
    private final String STORAGE_PATH = "..\\cache";

    @Autowired
    private HistoryCsv historyCsv;

    public Optional<File> uploadGif(GetGifDto gif, String userId, Optional<String> query) throws IOException {
        File file = new File(STORAGE_PATH + query + "\\" + gif.getId() + ".gif");
        if (!userId.isEmpty()) {
            return putImageToUserFolder(file, userId, query);
        }
        return Optional.of(file);
    }

    public File putImageToUserFolder(Path path, String userId) {
        File source = new File(path.toString());
        File userFolder = new File(STORAGE_PATH + "\\users\\" + userId);
        try {
            userFolder.mkdirs();
            userFolder= new File(userFolder, source.getName());
            Files.copy(source.toPath(), userFolder.toPath());
            historyCsv.writeHistory(LocalDate.now(), userId, path.subpath(path.getNameCount() - 2,
                    path.getNameCount() - 1).toString(), source.getName());
        } catch (IOException ex) {
            throw new NotFoundException("file already exists");
        }
        return new File(userFolder, source.getName());
    }

    public Optional<File> putImageToUserFolder(File path, String userId, Optional<String> query) {
        File source = new File(path.toString());
        File userFolder = new File(STORAGE_PATH + "\\users\\" + userId + "\\" + query);
        try {
            userFolder.mkdirs();
            userFolder= new File(userFolder, source.getName());
            Files.copy(source.toPath(), userFolder.toPath());
            historyCsv.writeHistory(LocalDate.now(), userId, query.toString(), source.getName());
        } catch (IOException ex) {
            throw new NotFoundException("file already exists");
        }
        return Optional.of(new File(userFolder, source.getName()));
    }
}
