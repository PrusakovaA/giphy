package com.bsa.giphy.controller;


import com.bsa.giphy.service.CacheService;
import com.bsa.giphy.util.Validator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/cache")
@Slf4j
public class CacheController {

    @Autowired
    private CacheService cacheService;

    @Autowired
    private Validator validator;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<Map<String, Object>>> getAllCache(@RequestParam(required = false) String query) {
        validator.validate(Optional.ofNullable(query).orElse(""));
        log.info("Request to get all cache by : " + Optional.ofNullable(query).orElse(""));
        return cacheService.getAllCache(Optional.ofNullable(query));
    }

    @PostMapping("/generate")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<Map<String, Object>>> generateCache(@RequestParam String query) {
        log.info("Generate cache by : " + query);
        return cacheService.generateCache(query);
    }

    @DeleteMapping
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void clearCache() {
        log.info("Clear cache");
        cacheService.clearCache();
    }
}
