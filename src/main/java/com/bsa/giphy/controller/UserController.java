package com.bsa.giphy.controller;

import com.bsa.giphy.service.UserService;
import com.bsa.giphy.util.Validator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/user")
@Slf4j
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private Validator validator;

    @PostMapping("/{id}/generate")
    public String generateGif(@PathVariable String id, @RequestParam(required = false) Boolean force, @RequestParam String query) {
        validator.validate(id, query);
        log.info(id + " generate gif");
        return userService.generateGif(Optional.ofNullable(force), query, id);
    }

    @GetMapping("/{id}/all")
    public ResponseEntity<List<Map<String, Object>>> getAllFiles(@PathVariable String id) {
        validator.validate(id);
        log.info(id + " user's files");
        return userService.getAllFiles(id);
    }

    @GetMapping("/{id}/history")
    public File getUserHistory(@PathVariable String id) {
        validator.validate(id);
        log.info(id + " history");
        return userService.getUserHistory(id);
    }

    @DeleteMapping("/{id}/history/clean")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void cleanUserHistory(@PathVariable String id) {
        validator.validate(id);
        log.info("cleaned " + id + " history");
        userService.cleanUserHistory(id);
    }


    @DeleteMapping("/{id}/clean")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void cleanUser(@PathVariable String id) {
        validator.validate(id);
        log.info(id + " requests clean his data");
        userService.deleteUser(id);
    }

}
