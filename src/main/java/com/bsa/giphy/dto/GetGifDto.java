package com.bsa.giphy.dto;

import lombok.Getter;
import lombok.Setter;

import java.net.URL;

@Getter
@Setter
public class GetGifDto {
    private String id;
    private URL url;
}
