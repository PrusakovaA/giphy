package com.bsa.giphy.exception;

public class InvalidException  extends RuntimeException {
    public InvalidException(String message) {
        super(message);
    }
}
