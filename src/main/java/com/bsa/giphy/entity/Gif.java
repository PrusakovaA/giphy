package com.bsa.giphy.entity;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.io.File;
import java.util.Optional;

@Getter
@Setter
@Builder
public class Gif {
    private String userId;
    private Optional<String> query;
    private Optional<File> gif;
    private String name;
}
