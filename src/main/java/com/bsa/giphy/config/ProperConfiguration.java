package com.bsa.giphy.config;

import com.bsa.giphy.entity.Gif;
import com.bsa.giphy.util.GetGif;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties({GifConfiguration.class})
public class ProperConfiguration {

    @Bean
    public GetGif gif (GifConfiguration gifConfiguration) {
        return new GetGif(gifConfiguration.getUrl(), gifConfiguration.getKey());
    }
}
