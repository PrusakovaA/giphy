package com.bsa.giphy.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "giphy")
@Getter
@Setter
public class GifConfiguration {
    private String url;
    private String key;
}
